import Constants from "../../../utils/constants/constants";

export class ErrorFormat {
  code: number;
  message: string;
  cause?: string;
  data?: any;
  internalCode?: string;

  constructor(
    code: number,
    message: string,
    cause = "",
    data = {},
    internalCode = ""
  ) {
    this.code = code;
    this.message = message;
    this.cause = cause;
    this.data = data;
    this.internalCode = internalCode;
  }
}

export class NotFoundError extends ErrorFormat {
  constructor(cause = "", data = {}, internalCode = "") {
    super(
      Constants.HTTP.RESPONSE.STATUS.NOT_FOUND_ERROR.CODE,
      Constants.HTTP.RESPONSE.STATUS.NOT_FOUND_ERROR.MESSAGE,
      cause,
      data,
      internalCode
    );
  }
}

export class InvalidParameterError extends ErrorFormat {
  constructor(cause = "", data = {}, internalCode = "") {
    super(
      Constants.HTTP.RESPONSE.STATUS.INVALID_PARAMETERS.CODE,
      Constants.HTTP.RESPONSE.STATUS.INVALID_PARAMETERS.MESSAGE,
      cause,
      data,
      internalCode
    );
  }
}
