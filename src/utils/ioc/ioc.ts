import { Container, inject } from "inversify";
import { makeFluentProvideDecorator } from "inversify-binding-decorators";

let iocContainer = new Container();
let provideFluent = makeFluentProvideDecorator(iocContainer);

let provideSingleton = (identifier: any) => {
  return provideFluent(identifier)
    .inSingletonScope()
    .done();
};

export { iocContainer, provideSingleton, inject };
