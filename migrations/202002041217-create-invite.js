'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Invite', {
            id: {
                allowNull: false,
                autoIncrement: true,
                type: Sequelize.INTEGER
            },
            nom: {
                type: Sequelize.STRING
            },
            prenom: {
                type: Sequelize.STRING
            },
            table: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            gender: {
                type: Sequelize.STRING
            },
            dateModification: {
                type: Sequelize.STRING
            }
        })
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Invite')
    }
}