import { provideSingleton } from "../../../utils/ioc/ioc";
import { ErrorFormat } from "../models/error-format";

@provideSingleton(AbstractController)
export class AbstractController {
  protected handleError(err: any) {
    if (err instanceof ErrorFormat) {
      return Promise.resolve({
        statusCode: err.code,
        response: {
          message: err.message,
          cause: err.cause,
          data: err.data,
          internalCode: err.internalCode
        }
      });
    } else {
      return Promise.resolve({
        statusCode: err.code,
        response: {
          message: err.message
        }
      });
    }
  }
}
