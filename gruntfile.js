module.exports = function (grunt) {
    "use strict";

    grunt.initConfig({
        copy: {
            buid: {
                files: [{
                        expand: true,
                        cwd: "./public",
                        src: ["**"],
                        dest: "./dist/public"
                    },
                    {
                        expand: true,
                        cwd: "./views",
                        src: ["**"],
                        dest: "./dist/views"
                    },
                    {
                        expand: true,
                        cwd: "./src",
                        src: ["utils/database/models/*"],
                        dest: "./dist"
                    }
                ]
            }
        },
        ts: {
            app: {
                tsconfig: true,
                files: [{
                    src: ["src/**/*.ts", "!src/.baseDir.ts"],
                    dest: "./dist"
                }],
                options: {
                    module: "commonjs",
                    target: "es6",
                    sourceMap: true,
                    rootDir: "src"
                }
            }
        },
        watch: {
            ts: {
                files: ["src/**/*.ts"],
                tasks: ["ts"]
            },
            views: {
                files: ["views/**/*.pug"],
                tasks: ["copy"]
            }
        },
        availabletasks: {
            tasks: {
                options: {
                    filter: "exclude",
                    tasks: ["availabletasks", "tasks"]
                }
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-available-tasks");
    grunt.registerTask("tasks", ["availabletasks"]);
    grunt.registerTask("default", ["copy", "ts"]);
};