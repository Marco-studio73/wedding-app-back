"use strict";
module.exports = (sequelize, DataTypes) => {
    const Table = sequelize.define('Table', {
        name: DataTypes.STRING,
        type: DataTypes.INTEGER,
        nbInvites: DataTypes.INTEGER
    }, {
        freezeTableName: true,
        timestamps: false
    });
    Table.associate = function (models) {

    };

    return Table;
}