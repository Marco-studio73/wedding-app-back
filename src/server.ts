/* tslint:disable */
/**
 * Server module.
 * Main class of Application Server
 * @module server
 */

import * as configuration from "config";

// Load dependencies

import * as bodyParser from "body-parser";

import * as cookieParser from "cookie-parser";
import * as cors from "cors";
import * as errorHandler from "errorhandler";
import * as express from "express";
import * as httpContext from "express-http-context";
import * as _ from "lodash";
import * as methodOverride from "method-override";
import * as moment from "moment";
import * as morgan from "morgan";
import * as os from "os";
import "reflect-metadata";
import * as swaggerUi from "swagger-ui-express";
import * as routerApiV1 from "./api/v1/routes/routes";
import { inject, iocContainer, provideSingleton } from "./utils/ioc/ioc";
import { SequelizeService } from "./api/v1/services/sequelize-service";

const app = express();

// options for cors midddleware
const options: cors.CorsOptions = {
  allowedHeaders: [
    "Content-Type",
    "Authorization",
    "Origin",
    "Access-Control-Allow-Origin",
    "X-Requested-With",
    "Accept",
    "X-Access-Token",
    "requestToken",
    "X-Request-Token",
    "x-poste-ip",
    "x-date-production"
  ],
  credentials: true,
  methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  optionsSuccessStatus: 200,
  origin: true,
  preflightContinue: false
};
const swaggerFile = "/swagger.json";
const optionsSwagger = {
  swaggerUrl: configuration.get("host") + `${swaggerFile}`,
  validatorUrl: null
};

@provideSingleton(Server)
export class Server {
  @inject(SequelizeService)
  private readonly sequelizeService: SequelizeService;

  public static bootstrap(): Server {
    const servers: Server = iocContainer.get<Server>(Server);
    servers.config();
    return servers;
  }

  private config() {
    // Test environment: development or production
    const nodeEnv = _.isUndefined(process.env.NODE_ENV)
      ? "local"
      : process.env.NODE_ENV;

    // Test for PORT environment variable
    const port = configuration.get("port");
    const hostname = os.hostname();

    // Define options for router//Initialize Express application
    app.use(cors(options));

    // Swagger
    app.use("/docs", swaggerUi.serve, swaggerUi.setup(null, optionsSwagger));
    app.use(`${swaggerFile}`, (req, res) => {
      res.sendFile(__dirname + `${swaggerFile}`);
    });

    // **************************************
    // Initiate midleware for application
    // **************************************

    // configure morgan token
    morgan.token("date", (req, res) => {
      return moment().format("YYYY-MM-DDTHH:mm:ssZ");
    });
    // Log to console
    app.use(
      morgan(
        "[:date] :method :url :status :response-time ms - :res[content-length]"
      )
    );

    // Middleware to parse application/json and application/x-www-form-urlencoded
    app.use(bodyParser.json());
    app.use(
      bodyParser.urlencoded({
        extended: true
      })
    );

    // Middleware to parse cookie
    app.use(cookieParser());

    // Middleware to override HTTP verb
    app.use(methodOverride());

    // Middle ware to set http context
    app.use(httpContext.middleware);

    if (nodeEnv === "dev") {
      // errorHandler need to be run only on dev env
      // Middleware to handle error in express server
      app.use(errorHandler());
    }

    app.get("/", function(req, res) {
      res.send("Server running");
    });

    // Register Application routes
    routerApiV1.RegisterRoutes(app);

    // ***************************************
    //  Start Application
    // ***************************************
    const serv = app.listen(port, () => {
      // this.logger.getLogsLogger().log(Constants.LOGGER.LEVEL.INFO, 'Start Server');
      console.info(`!!! --------- Server started   ---------- !!!`);
      console.info(`Environment set to: ${nodeEnv}`);
      console.info(`    Server host:   ${hostname}`);
      console.info(`    Server port:   ${port}`);

      if (configuration.get("databaseConfig.service") === "on") {
        console.info(`Database connection`);
        console.info(
          `    Server dialect:   ${configuration.get("databaseConfig.dialect")}`
        );
        console.info(
          `    Server host:   ${configuration.get("databaseConfig.host")}`
        );
        console.info(
          `    Server port:   ${configuration.get("databaseConfig.port")}`
        );
        console.info(
          `    Server database:   ${configuration.get(
            "databaseConfig.database"
          )}`
        );
      }
    });

    process.on("SIGINT", function() {
      console.error("Caught SIGINT, shutting down");
      process.exit(0);
    });
  }
}

const server = Server.bootstrap();
export { server };
