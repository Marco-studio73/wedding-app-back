import Constant from "../../../utils/constants/constants";
import { provideSingleton } from "../../../utils/ioc/ioc";
import { ErrorFormat } from "../models/error-format";

@provideSingleton(AbstractService)
export class AbstractService {
  protected handleError(err: any) {
    if (err instanceof ErrorFormat) {
      throw err;
    } else {
      throw new ErrorFormat(
        Constant.HTTP.RESPONSE.STATUS.INTERNAL_ERROR.CODE,
        err.message
      );
    }
  }
}
