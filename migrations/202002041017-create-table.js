'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Table', {
            id: {
                allowNull: false,
                autoIncrement: true,
                type: Sequelize.INTEGER,
                primaryKey: true,
            },
            code: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true
            },
            name: {
                type: Sequelize.STRING
            },
            type: {
                type: Sequelize.INTEGER
            },
            nbInvites: {
                type: Sequelize.INTEGER
            }
        })
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Table')
    }
}