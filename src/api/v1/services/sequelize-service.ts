import * as configuration from "config";
import * as fs from "fs";
import * as path from "path";
import * as Sequelize from "sequelize";
import { inject, provideSingleton } from "../../../utils/ioc/ioc";

@provideSingleton(SequelizeService)
export class SequelizeService {
  public sequelize;
  dbModels = {};

  constructor(sequelize: any = null) {
    if (sequelize) {
      this.sequelize = sequelize;
    } else {
      this.connectToDB();
    }

    this.importModels();
  }

  connectToDB() {
    this.sequelize = new Sequelize(
      configuration.get("databaseConfig.database"),
      configuration.get("databaseConfig.username"),
      configuration.get("databaseConfig.password"),
      {
        host: configuration.get("databaseConfig.host"),
        port: configuration.get("databaseConfig.port"),
        dialect: configuration.get("databaseConfig.dialect"),
        operatorsAliases: false,
        pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
        },
        logging: false
      }
    );

    this.sequelize
      .authenticate()
      .then(() => {
        console.info("Connexion has been established successfully");
      })
      .catch((err: Error) => {
        console.error("Unable to connect to te database", err);
        this.connectToDB();
      });
  }

  importModels() {
    const modelsSequelizeLocation =
      __dirname + "/../../../utils/database/models/";
    fs.readdirSync(modelsSequelizeLocation)
      .filter(file => {
        return file.indexOf(".") !== 0 && file.slice(-3) === ".js";
      })
      .forEach(file => {
        const model = this.sequelize["import"](
          path.join(modelsSequelizeLocation, file)
        );
        this.dbModels[model.name] = model;
      });
    Object.keys(this.dbModels).forEach(modelName => {
      if (this.dbModels[modelName].associate) {
        this.dbModels[modelName].associate(this.dbModels);
      }
    });
  }
}
