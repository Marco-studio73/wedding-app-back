import * as _ from "lodash";
import constants from "../../../utils/constants/constants";
import { inject, provideSingleton } from "../../../utils/ioc/ioc";
import { InvalidParameterError, NotFoundError } from "../models/error-format";
import { AbstractService } from "./abstract-service";
import { InviteRepository } from "../repositories/invite-repository";
import { IInvite } from "../models/invite";

@provideSingleton(InviteService)
export class InviteService extends AbstractService {
  constructor(
    @inject(InviteRepository)
    private readonly inviteRepository: InviteRepository
  ) {
    super();
  }

  public getAllInvite(): Promise<IInvite[] | void> {
    return this.inviteRepository
      .findAll()
      .then((invites: IInvite[]) => {
        return invites;
      })
      .catch((err: any) => {
        super.handleError(err);
      });
  }

  // public getAllByDateProd(dateProduction: number): Promise<any> {
  //     return this.articleRepository.findAllMatchingCriteria({ dateProduction }).then(articles => {
  //         return articles;
  //     }).catch((err: any) => {
  //         super.handleError(err);
  //     });
  // }

  // public getById(id: number, dateProduction: number): Promise<any> {
  //     return this.articleRepository.findOneById(id, dateProduction).then(article => {
  //         return article;
  //     }).catch((err: any) => {
  //         super.handleError(err);
  //     });
  // }

  // public getByCodeArticle(codeArticle: string, dateProduction: number): Promise<any> {
  //     return this.articleRepository.findOneByCodeArticle(codeArticle, dateProduction).then(article => {
  //         return article;
  //     }).catch((err: any) => {
  //         super.handleError(err);
  //     });
  // }

  // public getWithLPByCodeArticle(codeArticle: string, type: AIM_ID, dateProduction: number): Promise<IArticle> {
  //     return this.articleRepository.findManyByCodeArticleWithLP(codeArticle, dateProduction).then((articles: IArticle[]) => {
  //         if (articles.length > 0) {
  //             articles[0].typeArticle = type;
  //         }
  //         return articles;
  //     }).catch((err: any) => {
  //         super.handleError(err);
  //         return null;
  //     });
  // }

  // public getAllArticlesWithCriteria(articleCodes: string[], dateProduction: number): Promise<IArticle[] | []> {
  //     if (!_.isEmpty(articleCodes)) {
  //         return this.articleRepository.findAllWithArticlesCriteria(articleCodes, dateProduction)
  //             .then(articles => {
  //                 return articles;
  //             })
  //             .catch((err: any) => {
  //                 super.handleError(err);
  //             });
  //     } else {
  //         return Promise.resolve([]);
  //     }
  // }

  // public getAllArticlesByArticleCodes(articleCodes: string[]): Promise<IArticle[]> {
  //     return this.articleRepository.findAllWithCodeArticle(articleCodes).catch(err => {
  //         this.handleError(err);
  //         return [];
  //     });
  // }

  // public getCodeRayonCsdByDateProdAndGroupeRayon(dateProd: number, groupeRayon: string): Promise<string[]> {
  //     return this.articleRepository.findCodeRayonCsdWithDateProdAndGroupeRayon(dateProd, groupeRayon).then(articles => {
  //         return articles.map(article => {
  //             return article.codeRayonCsd;
  //         });
  //     }).catch(err => {
  //         this.handleError(err);
  //         return [];
  //     });
  // }

  // public getArticleCodeLibelleForManquants(dateProd: number, groupeRayon: string, rayon: string): Promise<IArticleForManquantCodeLibelle[]> {
  //     return this.articleRepository.findAllArticleCodeLibelleForManquants(dateProd, groupeRayon, rayon).catch(err => {
  //         this.handleError(err);
  //         return [];
  //     });
  // }

  // public create(article: IArticle): Promise<any> {
  //     return this.articleRepository.create(article).then((response: IArticle) => {
  //         return response;
  //     }).catch((err: any) => {
  //         super.handleError(err);
  //     });
  // }

  // public update(id: number, article: IArticle): Promise<any> {
  //     if (id !== article.id) {
  //         throw new InvalidParameterError(constants.ARTICLE.ERRORS.ID_NOT_FOUND);
  //     }
  //     return this.articleRepository.update(article).then((response: IArticle) => {
  //         if (!_.isEmpty(response)) {
  //             return response;
  //         } else {
  //             throw new NotFoundError(`${constants.ARTICLE.ERRORS.ID_NOT_FOUND} ${id}`);
  //         }
  //     }).catch((err: any) => {
  //         super.handleError(err);
  //     });
  // }

  // public duplicateArticleForT50(codeArticle: string, dateProduction: number, dateProductionInitiale: number, refTrieur: string): Promise<IArticle> {
  //     return this.getByCodeArticle(codeArticle, dateProduction).then(article => {
  //         if (_.isEmpty(article)) {
  //             return this.getByCodeArticle(codeArticle, dateProductionInitiale).then(articleADupliquer => {
  //                 if (!_.isEmpty(articleADupliquer)) {
  //                     articleADupliquer.dataValues.dateProduction = dateProduction;
  //                     articleADupliquer.dataValues.referenceTrieurSequence = refTrieur;
  //                     delete articleADupliquer.dataValues.id;
  //                     return this.create(articleADupliquer.dataValues).catch((err: any) => {
  //                         throw err;
  //                     });
  //                 } else {
  //                     return null;
  //                 }
  //             }).catch((err: any) => {
  //                 throw err;
  //             });
  //         } else {
  //             return article;
  //         }
  //     }).catch((err: any) => {
  //         this.handleError(err);
  //     });
  // }

  // public createMany(articles: IArticle[]): Promise<any> {
  //     return this.articleRepository.createMany(articles).then((response: IArticle[]) => {
  //         return response;
  //     }).catch((err: any) => {
  //         super.handleError(err);
  //     });
  // }

  // public deleteArticlesByTopOriginAndReference(ref: string, top: number): Promise<number> {
  //     return this.articleRepository.deleteArticlesByTopOriginAndReference(ref, top).catch((err: any) => {
  //         this.handleError(err);
  //         return 0;
  //     });
  // }

  // public getFournisseurLabelFromCode(codeFournisseur: string, dateProduction: number): Promise<string | void> {
  //     return this.articleRepository.getFournisseurLabelFromCode(codeFournisseur, dateProduction).catch((err) => {
  //         this.handleError(err);
  //     });
  // }

  // public selectArticleWithSpecialOffer(articles: IArticle[], dateProduction: number): Promise<IArticle> {
  //     return this.articleRepository.selectArticleWithSpecialOffer(articles, dateProduction).then((article: IArticle) => {
  //         return article ? article : articles[0];
  //     });
  // }
}
