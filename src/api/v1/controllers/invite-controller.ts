import { Get, Header, Route, Tags, Post, Query, Body } from "tsoa";
import { inject, provideSingleton } from "../../../utils/ioc/ioc";
import { IControllerResponse } from "../models/controller-response";
import { AbstractController } from "./abstract-controller";
import { IInvite } from "../models/invite";
import { InviteService } from "../services/invite-service";

@Route("invite")
@provideSingleton(InviteController)
export class InviteController extends AbstractController {
  constructor(
    @inject(InviteService) private readonly inviteService: InviteService
  ) {
    super();
  }

  /**
   * obtenir la liste de tous les invites
   *
   * @summary Obtenir la liste de tous les invités
   */

  @Get("")
  @Tags("invite")
  public async getAllInvite(): Promise<IControllerResponse<IInvite[] | void>> {
    return this.inviteService.getAllInvite().then((listInvite: IInvite[]) => {
      return { statusCode: 200, response: listInvite };
    });
  }

  // /**
  //  * Ajouter le status adequat au colis servis
  //  * @summary MAJ du statut du colis servis
  //  */

  // @Post('/setStatusAnomalie')
  // @Tags('anomalie')

  // public async setStatutToAnomalie(@Body() statutAndSsccColis: StatutAndSsccColis): Promise<IControllerResponse<number | void>> {
  //     return this.anomalieService.updateStatutColisServis(statutAndSsccColis.ssccColis, statutAndSsccColis.statut).then((colisUpdated: number) => {
  //         return { statusCode: 200, response: colisUpdated };
  //     }).catch((err: Error) => {
  //         return this.handleError(err);
  //     });
  // }

  // /**
  //  * Pouvoir afficher les informations d'un colis mis en attente our refuser depuis la liste des colis servis
  //  * @summary Trouver un colis mis en attente ou refuser
  //  */

  // @Get('/findColisServisMEAorRefused')
  // @Tags('anomalie')
  // public async findColisServisMEAorRefused(@Header('x-date-production') dateProd: number, @Query() ssccColis: string): Promise<IControllerResponse<IColisAnomalie[] | void>> {
  //     return this.anomalieService.getColisServiAnomalieByDateProduction(dateProd, undefined, ssccColis).then((colisServisFinded: IColisAnomalie[]) => {
  //         return { statusCode: 200, response: colisServisFinded };
  //     }).catch((err: Error) => {
  //         return this.handleError(err);
  //     });

  // }
}
