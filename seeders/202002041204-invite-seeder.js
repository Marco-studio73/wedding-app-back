'use strict';

const moment = require('moment');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Invite',
            [{
                    "nom": "Pugh",
                    "prenom": "Martin",
                    "gender": "male",
                    "table": "",
                    "dateModification": "Sat Mar 21 2020 01:49:52 GMT+0100 (heure normale d’Europe centrale)"
                },
                {
                    "nom": "Susanne",
                    "prenom": "Salinas",
                    "gender": "female",
                    "table": "",
                    "dateModification": "Wed Apr 25 2018 19:41:02 GMT+0200 (heure d’été d’Europe centrale)"
                },
                {
                    "nom": "Elaine",
                    "prenom": "Meadows",
                    "gender": "female",
                    "table": "",
                    "dateModification": "Mon May 19 2014 20:34:31 GMT+0200 (heure d’été d’Europe centrale)"
                },
                {
                    "nom": "Duncan",
                    "prenom": "Rosales",
                    "gender": "male",
                    "table": "",
                    "dateModification": "Thu May 30 2019 05:38:08 GMT+0200 (heure d’été d’Europe centrale)"
                },
                {
                    "nom": "Aimee",
                    "prenom": "Becker",
                    "gender": "female",
                    "table": "",
                    "dateModification": "Tue Aug 20 2019 00:30:36 GMT+0200 (heure d’été d’Europe centrale)"
                },
                {
                    "nom": "Marshall",
                    "prenom": "Freeman",
                    "gender": "male",
                    "table": "",
                    "dateModification": "Sat Aug 29 2015 17:08:59 GMT+0200 (heure d’été d’Europe centrale)"
                },
                {
                    "nom": "Pope",
                    "prenom": "Bray",
                    "gender": "male",
                    "table": "",
                    "dateModification": "Wed Nov 28 2018 02:18:27 GMT+0100 (heure normale d’Europe centrale)"
                },
                {
                    "nom": "Solomon",
                    "prenom": "Heath",
                    "gender": "male",
                    "table": "",
                    "dateModification": "Sun Jan 08 2017 22:01:40 GMT+0100 (heure normale d’Europe centrale)"
                },
                {
                    "nom": "Myrna",
                    "prenom": "Wheeler",
                    "gender": "female",
                    "table": "",
                    "dateModification": "Mon Jul 17 2017 07:39:37 GMT+0200 (heure d’été d’Europe centrale)"
                },
                {
                    "nom": "Bowman",
                    "prenom": "Richmond",
                    "gender": "male",
                    "table": "",
                    "dateModification": "Tue Mar 01 2016 17:15:54 GMT+0100 (heure normale d’Europe centrale)"
                }
            ], {})
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Invite', null, {});
    }
};