import * as _ from "lodash";
import * as moment from "moment";
import * as sequelize from "sequelize";
import { Op } from "sequelize";
import constants from "../../../utils/constants/constants";
import { inject, provideSingleton } from "../../../utils/ioc/ioc";
import { SequelizeService } from "../services/sequelize-service";
import { AbstractRepository } from "./abstract-repository";
import { IInvite } from "../models/invite";

@provideSingleton(InviteRepository)
export class InviteRepository extends AbstractRepository {
  constructor(@inject(SequelizeService) sequelizeService: SequelizeService) {
    super(sequelizeService);
  }

  protected defineModel(): void {
    this.model = this.sequelizeService.dbModels[
      constants.DATABASE.MODEL_KEY.INVITE
    ];
  }

  public findAll(): Promise<IInvite[] | void> {
    return this.model.findAll().then((listInvites: IInvite[]) => {
      return listInvites;
    });
  }

  // find one wich match criteria
  // criteria should be object with fields of article
  // public findAllMatchingCriteria(criteria: IJsonObject): Promise<any> {
  //     return this.model.findAll({ where: criteria }).then(articles => {
  //         return articles;
  //     });
  // }

  // public findAllWithArticlesCriteria(articles: string[], dateProduction: number): Promise<any> {
  //     if (!_.isEmpty(articles)) {
  //         return this.model.findAll({
  //             where: {
  //                 codeArticle: { [Op.in]: articles },
  //                 dateProduction: dateProduction
  //             }
  //         }).then(matchingArticles => {
  //             return matchingArticles.map(article => article ? article.dataValues : null);
  //         }).catch(err => {
  //             throw err;
  //         });
  //     }
  // }

  // public findAllWithCodeArticle(articles: string[]): Promise<IArticle[]> {
  //     return this.model.findAll({
  //         where: {
  //             codeArticle: { [Op.in]: articles },
  //         }
  //     }).catch(err => {
  //         this.handleError(err);
  //     });
  // }

  // public findCodeRayonCsdWithDateProdAndGroupeRayon(dateProd: number, groupeRayon: string): Promise<IArticle[]> {
  //     return this.model.findAll({
  //         attributes: [[sequelize.fn('DISTINCT', sequelize.col(constants.ARTICLE.CODE_RAYON_CSD)), constants.ARTICLE.CODE_RAYON_CSD]],
  //         where: {
  //             dateProduction: dateProd,
  //             codeGroupeRayon: groupeRayon
  //         }
  //     }).catch(err => {
  //         this.handleError(err);
  //     });
  // }

  // public findAllArticleCodeLibelleForManquants(dateProduction: number, codeGroupeRayon: string, codeRayonCsd: string): Promise<IArticleForManquantCodeLibelle[]> {
  //     let criteria: IParams = {};
  //     criteria.dateProduction = dateProduction;
  //     if (codeGroupeRayon) {
  //         criteria.codeGroupeRayon = codeGroupeRayon;
  //     }
  //     if (codeRayonCsd) {
  //         criteria.codeRayonCsd = codeRayonCsd;
  //     }
  //     return this.model.findAll({
  //         attributes: [constants.ARTICLE.CODE_ARTICLE, constants.ARTICLE.LIBELLE_ARTICLE, constants.ARTICLE.CODE_FOURNISSEUR, constants.ARTICLE.LIBELLE_FOURNISSEUR, [constants.ARTICLE.CODE_GROUPE_RAYON, constants.ARTICLE.GROUPE_RAYON]],
  //         where: criteria
  //     }).catch(err => {
  //         this.handleError(err);
  //     });
  // }

  // public findOneById(idArticle: number, dateProduction: number): Promise<any> {
  //     return this.model.findOne({
  //         where: { id: idArticle, dateProduction }
  //     }).then(article => {
  //         return article;
  //     });
  // }

  // public findOneByCodeArticle(searchedCode: string, dateProduction: number): Promise<IArticle> {
  //     return this.model.findOne({
  //         where: { codeArticle: searchedCode, dateProduction }
  //     }).then(article => {
  //         return article;
  //     });
  // }

  // public findManyByCodeArticleWithLP(searchedCode: string, dateProduction: number): Promise<IArticle[]> {
  //     let params = { searchedCode, dateProduction };
  //     let query = 'SELECT DISTINCT("Article".*) FROM "Article" JOIN "LignesPrepa" ON "Article"."codeArticle" = "LignesPrepa"."codeArticle" AND "Article"."codeArticle"= :searchedCode AND "LignesPrepa"."dateProduction" = :dateProduction AND "Article"."dateProduction" = :dateProduction AND ("nbColisAffecte" < "nbColisCommande" OR "nbColisAffecte" < "nbColis")';
  //     return this.select(query, params).then((articles: IArticle[]) => {
  //         return articles ? articles : null;
  //     }).catch((err) => {
  //         throw err;
  //     });
  // }
}
