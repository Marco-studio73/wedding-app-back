import Constants from "../../../utils/constants/constants";
import { inject, provideSingleton } from "../../../utils/ioc/ioc";
import { ErrorFormat } from "../models/error-format";
import { SequelizeService } from "../services/sequelize-service";
@provideSingleton(AbstractRepository)
export abstract class AbstractRepository {
  protected model;
  public sequelizeService;

  constructor(@inject(SequelizeService) sequelizeService: SequelizeService) {
    this.sequelizeService = sequelizeService;
    this.defineModel();
  }

  protected abstract defineModel(): void;

  protected rawQuery(query: string): Promise<any> {
    return this.sequelizeService.rawQuery().then(result => {
      return result;
    });
  }

  public select(query: string, params: any): Promise<any> {
    return this.sequelizeService.sequelize
      .query(query, {
        replacements: params,
        type: this.sequelizeService.sequelize.QueryTypes.SELECT
      })
      .then(response => {
        return response;
      });
  }

  protected handleError(err: any) {
    if (err instanceof ErrorFormat) {
      throw err;
    } else {
      throw new ErrorFormat(
        Constants.HTTP.RESPONSE.STATUS.INTERNAL_ERROR.CODE,
        err.message
      );
    }
  }
}
