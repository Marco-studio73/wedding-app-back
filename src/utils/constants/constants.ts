const constants = {
  DATE_MODIFICATION: "dateModification",

  DATE_FORMAT: {
    YYMMDD: "YYMMDD",
    YYYYMMDD: "YYYYMMDD",
    YYYYMMDDHHMMSS: "YYYYMMDDHHmmss"
  },

  ACTIONS: {
    ROUTER: {
      REQUEST: "REQUEST",
      RECORD: "RECORD"
    }
  },

  LABELS: {
    UI_SOCKET: {
      MESSSAGES: {
        CONNECT: "connect",
        MESSSAGE: "message",
        DISCONNECT: "disconnect",
        ERROR: "error"
      }
    }
  },
  HTTP: {
    HEADER: {
      REQUEST_TOKEN: "x-request-token",
      DATE_PRODUCTION: "x-date-production"
    },
    RESPONSE: {
      STATUS: {
        INTERNAL_ERROR: {
          CODE: 500
        },
        NOT_FOUND_ERROR: {
          CODE: 404,
          MESSAGE: "Resource not found"
        },
        UNAUTHORIZED: {
          CODE: 401,
          MESSAGE: "Unauthorized"
        },
        INVALID_PARAMETERS: {
          CODE: 409,
          MESSAGE: "Invalid request parameters"
        }
      }
    }
  },
  DATABASE: {
    MODEL_KEY: {
      INVITE: "Invite"
    }
  }
};

export default constants;
