export interface IControllerResponse<T> {
  statusCode: number;
  response: IErrorResponse | T;
}

export interface IErrorResponse {
  message: string;
  cause?: string;
  data?: Object;
  internalCode?: string;
}
