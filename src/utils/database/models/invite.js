"use strict";
module.exports = (sequelize, DataTypes) => {
  const Invite = sequelize.define(
    "Invite", {
      nom: DataTypes.STRING,
      prenom: DataTypes.STRING,
      gender: DataTypes.STRING,
      table: DataTypes.STRING,
      dateModification: DataTypes.STRING
    }, {
      freezeTableName: true,
      timestamps: false
    }
  );
  Invite.associate = function (models) {};

  return Invite;
};