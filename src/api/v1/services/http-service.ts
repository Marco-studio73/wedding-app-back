import * as httpContext from "express-http-context";
import * as rp from "request-promise";
import * as uuid from "uuid";
import constants from "../../../utils/constants/constants";
import { inject, provideSingleton } from "../../../utils/ioc/ioc";
import { AbstractService } from "./abstract-service";

@provideSingleton(HttpService)
export class HttpService extends AbstractService {
  constructor() {
    super();
  }

  public get(url: string, queryString?: any, headers?: any): Promise<any> {
    const requestToken = httpContext.get("requestToken") || uuid();

    return rp({
      ...this.getOptions("GET", url, requestToken, null, headers),
      qs: queryString
    })
      .then(elem => {
        return elem;
      })
      .catch(err => {
        return this.handlerError(err, "GET", requestToken);
      });
  }

  public post(url: string, body: any, headers?: any): Promise<any> {
    const requestToken = httpContext.get("requestToken") || uuid();
    return rp(this.getOptions("POST", url, requestToken, body, headers))
      .then(elem => {
        return elem;
      })
      .catch(err => {
        return this.handlerError(err, "POST", requestToken);
      });
  }

  public put(url: string, body: any, headers?: any): Promise<any> {
    const requestToken = httpContext.get("requestToken") || uuid();
    return rp(this.getOptions("PUT", url, requestToken, body, headers))
      .then(elem => {
        return elem;
      })
      .catch(err => {
        return this.handlerError(err, "PUT", requestToken);
      });
  }

  public delete(url: string, body: any, headers?: any): Promise<any> {
    const requestToken = httpContext.get("requestToken") || uuid();
    return rp(this.getOptions("DELETE", url, requestToken, body, headers))
      .then(elem => {
        return elem;
      })
      .catch(err => {
        return this.handlerError(err, "DELETE", requestToken);
      });
  }

  public getOptions(
    requestMethod: string,
    url: string,
    requestToken: string,
    body?: any,
    headers?: any
  ) {
    let formatedHeaders = {
      [constants.HTTP.HEADER.REQUEST_TOKEN]: requestToken
    };
    if (headers) {
      formatedHeaders = Object.assign({}, formatedHeaders, headers);
    }

    const options = {
      method: requestMethod,
      uri: url,
      json: true,
      headers: formatedHeaders
    };
    if (body) {
      const bodyKey = "body";
      options[bodyKey] = body;
    }
    return options;
  }

  private handlerError(error: any, action: string, requestToken: any) {
    const { statusCode, message } = error;
    throw error;
  }
}
